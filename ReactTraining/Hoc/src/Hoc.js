import React from "react";
import { Component } from "react/cjs/react.production.min";

class Hoc extends Component{
   state={
        gunshots:0
    }
    handleGunshots=()=>{
        this.setState({gunshots: this.state.gunshots+1})
    }
    render(){
        
        return(
            <div>
            <h3 onMouseOver={this.handleGunshots}>Hoc Gunshots:{this.state.gunshots}</h3>
        
            </div>
            
        )
        
        
    }
}
export default Hoc;