import"./App.css";
import React from "react";
import Navbar from "./Navbar";
function App() {
  return (
    <div class ="App">
      <div className="content">
        <h1 > App Component </h1>
        <Navbar />
      </div>
    </div>
  );
}
export default App;
