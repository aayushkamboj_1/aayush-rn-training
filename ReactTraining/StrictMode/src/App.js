import React from "react";
import "./styles.css";

const Fragments = () => {
  return(
    <React.StrictMode>
  <>
    <li className="list-group-item">Private Item 1</li>
    <li className="list-group-item">Private Item 2</li>
    <li className="list-group-item">Private Item 3</li>
  </>
  </React.StrictMode>
  )
}

export default Fragments
