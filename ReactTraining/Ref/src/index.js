import { StrictMode } from "react";
import ReactDOM from "react-dom";
import Ref from "./Ref";

import App from "./App";

const rootElement = document.getElementById("root");
ReactDOM.render(
  <StrictMode>
    <App />
    <Ref/>
  </StrictMode>,
  rootElement
);
