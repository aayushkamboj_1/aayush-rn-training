import React ,{ useEffect ,useState} from "react";
import "./styles.css";

const App= () => {
  
  const[state, setState]= useState(1);
  
  useEffect(() =>{
    alert ("i am clicking")
  },[])
  return(
    <div className ="App">
      <h1> Use Effect </h1>
      <button onClick={()=>setState(state+1)}> click me{state}</button>
      
      </div>
  );
}
export default App;
  
  
  
