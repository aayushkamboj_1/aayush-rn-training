import React from "react";
import Product from "./Product";

function App() {
  return (
    <div>
      <h1> PRODUCT </h1>
      <Product name="JBL" description="Speaker" price={60} />
    </div>
  );
}
export default App;
