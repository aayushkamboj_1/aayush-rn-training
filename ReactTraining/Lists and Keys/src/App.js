import React, { Component } from 'react';
import "./styles.css";
import Greet from './componets/Greet'
import Lists from './Lists';

class App extends Component {
  render (){
  return(
    <div className="App">
      <Greet />
      <Lists />
    </div>
  );
 }
}
 export default App;