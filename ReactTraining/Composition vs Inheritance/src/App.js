import "./styles.css";
import React from 'react';


export default class Calculator extends React.Component() {
  render() {
  return (
    <div>
      <h1>Composition vs inheritance</h1>
      <Sidebar data ={"sidebar props"}>
         <h1> data 1</h1> 
         <h1> data 2 </h1>
         <h1> data 3 </h1>
         <h1> data 4 </h1>
         
         </Sidebar>
    </div>
  );
 }
}   
   function Sidebar(props)
   {
     return <div>
        {props.children}
        </div>
   }
