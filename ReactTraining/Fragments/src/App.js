import "./styles.css";

const Fragments = () => {
  return(
  <>
    <li className="list-group-item">Private Item 1</li>
    <li className="list-group-item">Private Item 2</li>
    <li className="list-group-item">Private Item 3</li>
  </>
  )
}

export default Fragments
